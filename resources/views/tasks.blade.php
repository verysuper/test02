@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <form action="{{url('/task')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <span>Task add</span>
                            <input type="text" name="name">
                            <button>增加</button>
                        </form>
                    </div>
                    <div class="card-body">
                        <!-- Display Validation Errors -->
                        @include('common.errors')
                        <table class="table table-striped task-table">
                            @foreach ($tasks as $task)
                                <tr>
                                    <td class="table-text">
                                        <div>{{ $task->name }}</div>
                                    </td>
                                    <td>
                                        <form action="{{url('/task/'.$task->id)}}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button>刪除任務</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{ $tasks->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
