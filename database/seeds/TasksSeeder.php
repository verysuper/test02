<?php

use Illuminate\Database\Seeder;

class TasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1,10000000) as $id) {
            \App\Task::create([
                'user_id' => rand(1,4),
                'name' => 'Task'.$id
            ]);
        }
    }
}
